#include <vector>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
using namespace std;


class PeopleCircle
{
public:
  PeopleCircle() {}
  string order(int numMales, int numFemales, int K) {
    string s;
    int females = numFemales;
    int males = numMales;
    int i = 1;
    int total = males + females;
    int count = 1;
    for (int i = 0; i<numMales +numFemales; i++)
      {
	s += "M";
      }

    while (females > 0) {
      if (count == K) {
	if (i%total != 0)
	  s[i%total-1] =  'F';
	else
	  s[total-1] =  'F';

	count = 1;
	females--;
	
	i++;
	while (s[i%total-1] == 'F') 
	  i++;
      }	  
    
      if (s[i%total] != 'F') {
	count++;
	i++;
      }
      else {
	i++;
      }      
    }
    return s;
  }  
};
