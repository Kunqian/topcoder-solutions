#include <cmath>
#include <vector>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
#include <algorithm>    // std::sort
#include <gmp.h>
#include <math.h>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string.hpp>
using namespace std;



class VendingMachine {
 public:
  VendingMachine() {}

  bool elapsed_time;
  int current_column;
  int rotations;
  int expensive_column;

  void purchase(vector<string> &prices,int column, int shelf) {
    string s = prices[column];
    vector<string> v;
    boost::split(v, s, boost::is_any_of(" "));
    string zero = "0";
    v.insert(v.begin() + shelf, zero);
    string c;
    for (int i = 0; i<v.size(); i++)
      {
	if (i==v.size() -1)
	  c += v[i];
	else 
	  c += v[i]+" ";
      }
    prices.insert(prices.begin()+shelf, c);
  }

  int most_expensive_column(vector<string> prices) {
    int sum = 0;
    int max = 0;
    int column = 0;
    for (vector<string>::iterator it = prices.begin(); it != prices.end(); ++it)
      {
    	vector<string> s;
    	boost::split(s,*it,boost::is_any_of(" "));
    	for (vector<string>::iterator i = s.begin(); i!=s.end(); ++i)
    	  {
    	    sum += stoi(*i);
    	  }
    	if (sum > max) {
    	  column = distance(prices.begin() , it);
    	  max = sum;
    	}
      }
    return column;
  }

  int get_cheapest_rotations(vector<string> prices, int from, int to) {
    vector<string>::iterator to_pos = find(prices.begin(), prices.end(), prices[to]);
    vector<string>::iterator from_pos = find(prices.begin(), prices.end(), prices[from]);
    if (to_pos < from_pos) {
      int forward = distance(to_pos, from_pos);
      int a = distance(prices.begin(), to_pos);
      int b = distance(from_pos, prices.end());
      int backward = a + b;
      if (forward < backward) {
	return forward;
      }
      else {
	return backward;
      }
    }
    else {
      vector<string>::iterator tmp = to_pos;
      to_pos = from_pos;
      from_pos = tmp;
      int forward = distance(to_pos, from_pos);
      int a = distance(prices.begin(), to_pos);
      int b = distance(from_pos, prices.end());
      int backward = a + b;
      if (forward < backward) {
	return forward;
      }
      else {
	return backward;
      }     
    }

    return -1;
  }


  int motorUse(vector<string> prices, vector<string> purchases) {
    current_column = 0;
    elapsed_time = 0;
    expensive_column = most_expensive_column(prices);
    rotations = get_cheapest_rotations(prices, current_column, expensive_column);
    current_column = expensive_column;
    for (vector<string>::iterator it = purchases.begin(); it != purchases.end(); ++it)
      {	
	vector<string> s;
	boost::split(s,*it, boost::is_any_of(" ,:"));
	int dest_column = stoi(s[1]);
	int shelf = stoi(s[0]);
	int time = stoi(s[2]);
	elapsed_time = time - elapsed_time;
	if (elapsed_time >= 5*60) {
	  expensive_column = most_expensive_column(prices);
	  rotations += get_cheapest_rotations(prices, current_column, expensive_column);
	  current_column = expensive_column;
	}
	rotations += get_cheapest_rotations(prices, current_column, dest_column);	
	current_column = dest_column;
	purchase(prices, current_column, shelf);
      }
    rotations += get_cheapest_rotations(prices, current_column, expensive_column);
    current_column = expensive_column;

    return rotations;
  }

};

int main() {

  vector<string> s{"100", "100", "100"};
  vector<string> p{"0,0:0", "0,2:5", "0,1:10"};

  VendingMachine v;
  cout<<  v.motorUse(s,p)<<endl;

}
