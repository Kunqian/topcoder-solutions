#include <vector>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
using namespace std;

class BinaryCode
{
public:
  BinaryCode() {}
  string int_array_to_string(int int_array[], int size_of_array) {
    ostringstream oss("");
    for (int temp = 0; temp < size_of_array; temp++) {
      oss << int_array[temp];
    }
    return oss.str();
  }
  
  void string_to_int_array(string str, int int_array[]) {
    for (int i = 0; i<str.length(); i++) {
      int_array[i] = str[i] - '0';
    }
  }
  
  string decryptBinaryString(int q[], int len, int initval) {
    
    int p[len];
    p[0] = initval;
    for (int i = 0; i<len-1; i++) {
      if (i >= 2)
	p[i+1] = q[i] - q[i-1] + p[i-2];
      else if (i == 1)
	p[i+1] = q[i] - q[i-1];
      else if (i == 0)
	p[i+1] = q[0] - p[0];
      
      if (p[i+1] > 1 || p[i+1] < 0) {
	string result = "NONE";
	return result;
      }
    }  
  
    return int_array_to_string(p,len);    
  }
  
  vector<string> decode(string message) {
    vector<string> pair;
    int code[message.length()];
    string_to_int_array(message,code);
    string s0 = decryptBinaryString(code, message.length(), 0);
    string s1 = decryptBinaryString(code, message.length(), 1);
    pair.push_back(s0);
    pair.push_back(s1);
    return pair;
  }

};

int main() {
  // string str;
  // getline(cin,str);
  // int code[str.length()];
  // string_to_int_array(str,code);
  // string s0 = decryptBinaryString(code, str.length(), 0);
  // string s1 = decryptBinaryString(code, str.length(), 1);
  // cout<<"{ "<<"\""<<s0<<"\""<<",  "<<"\""<<s1<<"\""<<" }"<<endl;
}
