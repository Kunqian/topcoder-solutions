#include <vector>
#include <string>
#include <iostream>
using namespace std;


class Dragons
{
public:
  Dragons() {}
  
  void fill_neighbour(double n[][4], vector<double> f) {
    for (int i = 0; i < 6; i++) {
      if (i == 0) {
	n[i][0] = f[2];
	n[i][1] = f[3];
	n[i][2] = f[4];
	n[i][3] = f[5];
      }
      if (i == 1) {
	n[i][0] = f[2];
	n[i][1] = f[3];
	n[i][2] = f[4];
	n[i][3] = f[5];
      }
      if (i == 2) {
	n[i][0] = f[0];
	n[i][1] = f[1];
	n[i][2] = f[4];
	n[i][3] = f[5];
      }
      if (i == 3) {
	n[i][0] = f[0];
	n[i][1] = f[1];
	n[i][2] = f[4];
	n[i][3] = f[5];
      }
      if (i == 4) {
	n[i][0] = f[0];
	n[i][1] = f[1];
	n[i][2] = f[2];
	n[i][3] = f[3];
      }
      if (i == 5) {
	n[i][0] = f[0];
	n[i][1] = f[1];
	n[i][2] = f[2];
	n[i][3] = f[3];
      }
    }
  }

  vector<double> snaug(vector<int> initialFood, int round) {
    double neighbours[6][4];
    vector<double> f;
    vector<double> d{0,0,0,0,0,0};
    for (int i = 0; i<6; i++) {
      f.push_back(initialFood[i]);
    }
    long de = 1;

    for (int j=0; j<round; j++) {
      de *= 2;
      for (int i = 0; i<6; i++) {
  	fill_neighbour(neighbours, f);
	d[i] =0;
  	for (int k = 0; k<4; k++) {
  	  d[i] += neighbours[i][k];	
  	}
	d[i] /= 2;
      }
      f = d;
    }
    return d;
  }  
};


int main(int argc, char *argv[])
{
  Dragons d;
  vector<int> v{1,2,3,4,5,6};
  vector<double> f = d.snaug(v,1);
  for (int i = 0; i<6; i++) {
    cout<<f[i]<<endl;
  }
  return 0;
}
