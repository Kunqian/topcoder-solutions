#include <iostream>
#include <vector>
#include <cmath>
#include <vector>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
#include <algorithm>    // std::sort
#include <gmp.h>
#include <math.h>

using namespace std;


class YahtzeeScore
{
public:
  YahtzeeScore() {}
  int maxPoints(vector<int> toss) {
    int sum[6] = {0};
    int max = 0;
    for (int i = 0; i<5; i++)
      {
	if (toss[i] == 1) {
	  sum[0] += 1;
	}
	if (toss[i] == 2) {
	  sum[1] += 2;
	}
	if (toss[i] == 3) {
	  sum[2] += 3;
	}
	if (toss[i] == 4) {
	  sum[3] += 4;
	}
	if (toss[i] == 5) {
	  sum[4] += 5;
	}
	if (toss[i] == 6) {
	  sum[5] += 6;
	}
	for (int j = 0; j<6; j++)
	  {
	    if (sum[j] > max) {
	      max = sum[j];
	    }	    
	  }
      }
    return max;
  }
};


int main(int argc, char *argv[])
{
  YahtzeeScore y;
  vector<int> v{6,4,1,1,3};
  cout<<y.maxPoints(v)<<endl;
  return 0;
}
