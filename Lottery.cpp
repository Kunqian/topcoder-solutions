#include <cmath>
#include <vector>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
#include <algorithm>    // std::sort
#include <gmp.h>
#include <math.h>

using namespace std;

class Lottery {
public:
  Lottery() {};
  long long combination(long long iN, long long iR) {
    if (iR < 0 || iR > iN) {
      return 0;
    }
    long long iComb = 1;
    long long i = 0;
    while (i < iR) {
      ++i;
      iComb *= iN - i + 1;
      iComb /= i;
    }
    return iComb;
  }
  
  
  long long sort_combination(long long n, long long p) {
    // cout<<" "<<n-1+p<<" "<<p<<" "<<combination(n-1+p, p)<<endl;
    return combination(n-1+p, p);
  }

  long long all_combination(long long n, long long p) {
    if (p == 0)
      return 1;
    else
      return n * all_combination(n , p-1);
  }
  
  long long distinct_combination(long long n, long long k) {
    if (k == 0) {
      return 1;
    }
    else if (n == 0) {
      return 1;
    }
    else {
      return n * distinct_combination(n-1,k-1);
    }
  }

  long long sorted_and_distinct(long long n, long long k) {
    long long result = combination(n,k) ;
    return result;
  }

  struct rule {
    int choices;
    int blanks;
    bool sorted;
    bool unique;
    float probability;
    string s;

    bool operator() (rule r1, rule r2) {
      if (r2.probability == r1.probability) {
	return r2.s > r1.s;
      }
      else {
	return r2.probability < r1.probability;	
      }
    }

  };
 
  rule get_rule_result(string m) {
    rule r;
    double probability;
    int choices, blanks;
    bool sorted, unique;

    string::size_type colon_pos, begIdx, endIdx;
    colon_pos = m.find_first_of(":");
    r.s = m.substr(0,colon_pos);

    string rest = m.substr(colon_pos+1);
    vector<string> inputs;
    begIdx = rest.find_first_not_of(" ");
    while (begIdx != string::npos) {
      endIdx = rest.find_first_of(" ",begIdx);
      if (endIdx == string::npos) {
	endIdx = rest.length();
      }
      
      inputs.push_back(rest.substr(begIdx,endIdx - begIdx));
      begIdx = rest.find_first_not_of(" ", endIdx);
    }

    choices = stoi(inputs[0]);
    blanks = stoi(inputs[1]);

    if (inputs[2] == "F") {
      sorted = false;
    }
    else {
      sorted = true;
    }

    if (inputs[3] == "F") {
      unique = false;
    }
    else {
      unique = true;
    }
    probability = get_probability(choices, blanks, sorted, unique);

    r.choices = choices;
    r.blanks = blanks;
    r.sorted = sorted;
    r.unique = unique;
    r.probability = probability;
    return r;
  }
  
  float get_probability(int choices, int blanks, bool sorted, bool unique) {
    if (sorted == false && unique == false) {
      cout<<choices<<" "<<blanks<<" "<<all_combination(choices, blanks)<<endl; 
      return 1.0 / all_combination(choices, blanks);
    }
    else if (sorted == true && unique == false) {
      cout<<choices<<" "<<blanks<<" "<<sort_combination(choices, blanks)<<endl; 
      return 1.0 / sort_combination(choices, blanks);
    }
    else if (sorted == false && unique == true) {
      cout<<choices<<" "<<blanks<<" "<<distinct_combination(choices, blanks)<<endl; 
      return 1.0 / distinct_combination(choices, blanks);
    }
    else if (sorted == true && unique == true) {
      cout<<choices<<" "<<blanks<<" "<<sorted_and_distinct(choices, blanks)<<endl; 
      return 1.0 / sorted_and_distinct(choices, blanks);
    }
    else {
      return 0;
    }
  }

  vector<string> sortByOdds(vector<string> rules) {
    vector<rule> result;
    for (vector<string>::iterator it = rules.begin(); it != rules.end(); ++it) {
      rule r = get_rule_result(*it);
      result.push_back(r);
    }
    
    vector<string> retval;
    sort(result.begin(), result.end(), rule());
    for (vector<rule>::iterator it = result.begin(); it != result.end(); ++it) {
      retval.push_back(it->s);
    }
    
    return retval;
  }

};


int main() {
  Lottery l;
 
  vector<string> k;
  string INDIGO = "INDIGO: 93 8 T F";
  string ORANGE = "ORANGE: 29 8 F T";
  string VIOLET = "VIOLET: 76 6 F F";
  string BLUE = "BLUE: 100 8 T T";
  string RED = "RED: 99 8 T T";
  string GREEN = "GREEN: 78 6 F T";
  string YELLOW = "YELLOW: 75 6 F F";

  k.push_back(INDIGO);
  k.push_back(ORANGE);
  k.push_back(VIOLET);
  k.push_back(BLUE);
  k.push_back(RED);
  k.push_back(GREEN);
  k.push_back(YELLOW);
  
  vector<string> k2 = l.sortByOdds(k);
  
  for (vector<string>::iterator it = k2.begin(); it != k2.end(); ++it) {
    cout<<(*it)<<endl;
  }
}
