#include <vector>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
using namespace std;

class Time
{
public:
  Time() {};

  string whatTime(int seconds) {
    int r_hours = seconds / 3600;
    int r_minutes = (seconds % 3600) / 60;
    int r_seconds = seconds - 3600 * r_hours - 60 * r_minutes;    
    
    string s_hours = to_string(r_hours);
    string s_minutes = to_string(r_minutes);
    string s_seconds = to_string(r_seconds);

    string result = s_hours +":"+ s_minutes +":"+ s_seconds;
    return result;
  }
};


int main() {
  Time t;
  cout<<t.whatTime(5600)<<endl;
}
