#include <cmath>
#include <vector>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
#include <algorithm>    // std::sort
#include <gmp.h>
#include <math.h>
using namespace std;


class Bonuses
{
public:
  Bonuses() {};

  pair<int, int> get_start_pos(vector<int> b, int val, vector<int> taken) {
    int max = 0;
    pair<int, int> p = make_pair(0,0);
    for (int i = 0; i<b.size(); i++)
      {
	if ((b[i] > max && find(taken.begin(), taken.end(), i) == taken.end())) {
	  max = b[i];
	  p.first = b[i];
	  p.second = i;
	}
	else if ((b[i] == max && find(taken.begin(), taken.end(), i) == taken.end())) {
	  if (i < p.second) {
	    p.first = b[i];
	    p.second = i;
	  }
	}
      }
    return p;
  }

  vector<int> getDivision(vector<int> points) {
    int sum = 0;
    double totalBonus = 0;
    vector<int> bonuses;
    for (vector<int>::iterator it = points.begin(); it != points.end(); ++it)
      {	
	sum += *it;
      }
    for (int i =0; i < points.size(); i++)
      {
	double result = (double) points[i]/sum * 100;
	totalBonus += floor(result);
	bonuses.push_back(floor(result));
      }
    if ((100.0 - totalBonus) >= 1) {
       double r = 100.0 - totalBonus;
       int bonusInt = r;
       int current = 0;
       vector<int> tmp(bonuses);
       pair<int, int> pos = make_pair(-1,-1);
       vector<int> taken;
        if (bonusInt > 0) {
	  cout<<"Bonus: "<<bonusInt<<endl;
	  for (int i = 0; i<bonusInt; i++)
	   {
	     pos = get_start_pos(tmp,current,taken);
	     taken.push_back(pos.second);
	     current = pos.first;
	     // bonuses[pos.second] += 1;
	   }
        }
    }    
    return bonuses;
  }

};


int main() {
// {8, 6, 5, 2, 8, 5, 5, 3, 1, 5, 4, 4, 6, 3, 4, 4, 1, 8, 1, 6, 3, 8}
// {8, 6, 4, 2, 8, 5, 5, 3, 1, 4, 5, 4, 6, 3, 5, 4, 1, 8, 1, 6, 3, 8}
//        1                    1  1           1 

  Bonuses b;
  vector<int> v{487, 398, 223, 201, 60, 498, 130, 406, 279, 363, 291, 248, 445, 12, 156, 293};
  vector<int> r = b.getDivision(v);
  for (int i = 0; i<r.size(); ++i)
    {
      cout<<r[i]<<endl;
    }
}
