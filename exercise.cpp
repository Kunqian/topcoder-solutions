#include <cmath>
#include <vector>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
#include <algorithm>    // std::sort
#include <gmp.h>
#include <math.h>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string.hpp>
using namespace std;


class ExerciseMachine
{
public:
  ExerciseMachine() {};

  int getPercentages(string time) {
    int sum = 0;
    int showNum = 0;
    double percent = 0;
    vector<string> s;
    boost::split(s,time,boost::is_any_of(":"));
    sum = stoi(s[0]) * 3600 + stoi(s[1]) * 60 + stoi(s[2]);

    for (int i = 1; i<sum; i++) {
      percent =  (double) (i*100) / sum;
      if (percent == floor(percent)) {
	cout<<percent<<endl;
	showNum++;
      }
    }
    return showNum;
  }
};


int main() {

  ExerciseMachine e;
  cout<<e.getPercentages("00:14:10")<<endl;


}
