#include <string>
#include <vector>
#include <map>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <set>
#include <iostream>
#include <sstream>
#include <cstddef>
#include <algorithm>
#include <utility>
#include <iterator>
#include <numeric>
#include <list>
#include <complex>
 
using namespace std;
 
typedef vector<int> vi;
typedef vector<string> vs;
typedef long long ll;
typedef complex<double> pnt;
typedef pair<int, int> pii;
 
#define RA(x) (x).begin(), (x).end()
#define FE(i, x) for (typeof((x).begin()) i = (x).begin(); i != (x).end(); i++)
#define SZ(x) ((int) (x).size())
 
 
class RedIsGood
{
public:
    double getProfit(int R, int B);
};
 
static double row[2][5002];
 
double RedIsGood::getProfit(int R, int B)
{
    memset(row, 0, sizeof(row));
    int old = 0;
    int nu = 1;
    for (int i = 1; i <= R; i++)
    {
        for (int j = 0; j <= B; j++)
        {
            double pr = i / (double) (i + j);
            double pb = j / (double) (i + j);
            row[nu][j] = max(0.0, pr * (1.0 + row[old][j]) + pb * (row[nu][j - 1] - 1.0));
        }
        swap(old, nu);
    }
    
    for (int k = 0; k < 2; k++)
      {
	for (int l = 0; l < (R+B-1); ++l)
	  {
	    cout<<row[k][l]<<endl;
	  }
      }
    return row[old][B];
}

int main() {
  RedIsGood r;
  r.getProfit(2,2);
}
