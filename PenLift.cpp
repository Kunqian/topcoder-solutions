#include <cmath>
#include <vector>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
#include <algorithm>    // std::sort
#include <gmp.h>
#include <math.h>
using namespace std;


class PenLift
{
public:

  class Graph
  {
  public:
    int C;
    int V;
    int E;
    vector<int> *adj;

    Graph(int v) {
      V = v;
      E = 0;
      adj = new vector<int>[v];
    }

    Graph(vector<int> *adj_, int v, int e, int c) {
      C = c;
      V = v;
      E = e;
      adj = adj_;
    }
    
    int get_V() { return V; }
    int get_E() { return E; }

    void addEdge(int v, int w) 
    {
      adj[v].push_back(w);
      adj[w].push_back(v);
      E++;
    }

    vector<int> &get_adj(int v) {
       return adj[v];
    }

    int degree(int v) {
      return adj[v].size();
    }
  };

  class CC {
  public:
    bool *marked;
    int *id;
    int count;
    vector<int> components;

    CC(Graph &G) {
      count = 0;
      marked = new bool[G.get_V()];
      id = new int[G.get_V()];
      for (int s = 0; s < G.get_V(); s++) {
      	  if (!marked[s]) {
    	    int numOdd = 0;
      	    dfs(G, s, &numOdd);
	    if ((G.degree(s) * G.C) % 2 == 1) {
	      numOdd++;
	    }
    	    components.push_back(numOdd);
      	    count++;
      	  }
      }
    }
	       
    void dfs(Graph &G, int v, int *numOdd) {
      marked[v] = true;
      id[v] = count;

      for (vector<int>::iterator it = G.get_adj(v).begin(); it != G.get_adj(v).end(); ++it) {
    	if (!marked[*it]) {

    	  if (G.degree(*it) % 2 == 1) {
    	    *numOdd = *numOdd + 1;
    	  }

    	  dfs(G, *it, numOdd);
    	}
      }
    }

    bool connected(int v, int w) {
      return id[v] == id[w];
    }

    int get_id(int v) {
      return id[v];
    }

    int get_count() {
      return count;
    }
  };

  struct point {
    point() {}
    point(int _x, int _y) {
      x = _x;
      y = _y;
    }

    int x;
    int y;

    bool operator==(const point &p1) {
      if (p1.x == this->x && p1.y == this->y) {
	return true;
      }
      return false;
    }    
  };



  struct segment {
    segment() {}
    segment(point _p1, point _p2) {
      p1 = _p1;
      p2 = _p2;
    }
    point p1;
    point p2;
    static bool collinear (segment s1, segment s2) {
      return is_collinear(s1,s2);
    }

    friend ostream& operator<<(ostream& os, const segment &s) {
      os<<"("<<s.p1.x<<" "<<s.p1.y<<")"<<" "<<"("<<s.p2.x<<" "<<s.p2.y<<")";
      return os;
    }
  };



  static bool hasslope(segment s) {
    if (s.p1.x == s.p2.x) {
      return false;
    }
    else {
      return true;
    }
  }

  static double get_slope(segment s) {
    double slope = (double) (s.p1.y - s.p2.y) / (s.p1.x - s.p2.x);
    return slope;
  }

  static double get_b(segment s, double slope) {
    double b = (double) s.p1.y - slope * s.p1.x;
    return b;
  }


  static void combine(segment &s1, segment &s2) {
    int minx = min(min(s1.p1.x,s1.p2.x),min(s2.p1.x,s2.p2.x));
    int miny = min(min(s1.p1.y,s1.p2.y),min(s2.p1.y,s2.p2.y));
    int maxx = max(max(s1.p1.x,s1.p2.x),max(s2.p1.x,s2.p2.x));
    int maxy = max(max(s1.p1.y,s1.p2.y),max(s2.p1.y,s2.p2.y));
   
    s1.p1.x = minx;
    s1.p1.y = miny;
    s1.p2.x = maxx;
    s1.p2.y = maxy;

  }
  
  static int square(int x) {
    return x * x;
  }

  static double dist(const point &p1, const point &p2) {
    double d = sqrt(square(p1.x - p2.x) + square(p1.y - p2.y));
    return d;
  }

  static bool is_overlap(segment &s1, segment &s2) {
    if (dist(s1.p1, s2.p1) < dist(s2.p1, s2.p2) && dist(s1.p1, s2.p2) < dist(s2.p1, s2.p2)) {
      return true;
    }
    if (dist(s1.p2, s2.p1) < dist(s2.p1, s2.p2) && dist(s1.p2, s2.p2) < dist(s2.p1, s2.p2)) {
      return true;
    }

    if (dist(s2.p1, s1.p1) < dist(s1.p1, s1.p2) && dist(s2.p1, s1.p2) < dist(s1.p1, s1.p2)) {
      return true;
    }
    if (dist(s2.p2, s1.p1) < dist(s1.p1, s1.p2) && dist(s2.p2, s1.p2) < dist(s1.p1, s1.p2)) {
      return true;
    }

    return false;    
  }


  static bool is_collinear(segment &s1, segment &s2) {
    if (hasslope(s1) && hasslope(s2)) {
      double slope1 = get_slope(s1);
      double slope2 = get_slope(s2);

      if (abs(slope1 - slope2) < 0.000000001) {      
	double b1 = get_b(s1, slope1);
	double b2 = get_b(s2, slope2);
	if (abs(b1 - b2) < 0.000000001) {
	  if (is_overlap(s1,s2)) {
	    combine(s1,s2);
	    return true;
	  }
	  else {
	    return false;
	  }
	}
      }
      else {
	return false;
      }
    }
    else {
      if (hasslope(s1) || hasslope(s2)) {
	return false;
      }

      if (s1.p1.x == s2.p2.x) {
	if (is_overlap(s1,s2)) {	  
	  combine(s1,s2);
	  return true;
	}
	return false;
      }
      else {
	return false;
      }
    }
    return false;
  }
  
  static void merge_segments(vector<segment> &segments) {
    sort(segments.begin(), segments.end(), segment::collinear);
    segments.erase(unique(segments.begin(), segments.end(), is_collinear), segments.end());
  }


  segment parse_segment(string s) {
    segment s1;
    vector<string> result;
    string::size_type begIdx, endIdx;
    begIdx = s.find_first_not_of(" ");
    while (begIdx != string::npos) {
      endIdx = s.find_first_of(" ", begIdx);
      if (endIdx == string::npos) {
    	endIdx = s.length();
      }
      result.push_back(s.substr(begIdx,endIdx - begIdx));
      begIdx = s.find_first_not_of(" ", endIdx);
    }
    s1.p1.x = stoi(result[0]);
    s1.p1.y = stoi(result[1]);
    s1.p2.x = stoi(result[2]);
    s1.p2.y = stoi(result[3]);
    
    return s1;
  }
  
  vector<segment> parse_string(vector<string> segments) {
    vector<segment> result;
    for (vector<string>::iterator it = segments.begin(); it != segments.end(); ++it)
      {
	result.push_back(parse_segment(*it));
      }
    return result;
  }

  vector<int> *get_points(vector<segment> &s, int &length) {
    vector<point> points;
    vector<int> *neighbours = new vector<int>[s.size() * 2];
    vector<point>::iterator pos1, pos2;

    for (vector<segment>::iterator it = s.begin(); it != s.end(); ++it)
      {
    	pos1 = find(points.begin(), points.end(), it->p1);
    	pos2 = find(points.begin(), points.end(), it->p2);
	bool p1_found = pos1 != points.end();
	bool p2_found = pos2 != points.end();

	size_t index1 = distance(points.begin(), pos1);
	size_t index2 = distance(points.begin(), pos2);

    	if (!p1_found) { 
    	  points.push_back(it->p1);
	  index1 = points.size() - 1;
 
	  if (!p2_found) {
	    points.push_back(it->p2);
	    index2 = points.size() - 1;
	    neighbours[index1].push_back(index2);	 
	    neighbours[index2].push_back(index1);	  
	  }
	  else {
	    neighbours[index1].push_back(index2);	 
	    neighbours[index2].push_back(index1);
	  }
    	}
    	else {
	  if (p2_found) {
	    neighbours[index1].push_back(index2);	 
	    neighbours[index2].push_back(index1);	 	    
	  }
	  else {
	    points.push_back(it->p2);
	    index2 = points.size() - 1;
	    neighbours[index1].push_back(index2);	 
	    neighbours[index2].push_back(index1);	 	    	    
	  }
    	}	
      }
    length = points.size();
    return neighbours;
  }

  int numTimes(vector<string> segments, int n) {
    vector<segment> s = parse_string(segments);
    merge_segments(s);
    int size = 0;
    vector<int> *adj = get_points(s,size);
    Graph *G1 = new Graph(adj,size,s.size(),n);
    CC c(*G1);

    int sum = 0;
    for (vector<int>::iterator it=c.components.begin(); it != c.components.end(); ++it)
      {
	cout<<*it<<endl;
	if (*it > 2)
	  sum = sum + *it/2 - 1;
      }
    cout<<c.components.size()<<endl;
    return sum + c.components.size() - 1;
  }


  Graph *G;

  PenLift() {
 
    vector<string> v{
      "-252927 -1000000 -252927 549481","628981 580961 -971965 580961",
	"159038 -171934 159038 -420875","159038 923907 159038 418077",
	"1000000 1000000 -909294 1000000","1000000 -420875 1000000 66849",
	"1000000 -171934 628981 -171934","411096 66849 411096 -420875",
	"-1000000 -420875 -396104 -420875","1000000 1000000 159038 1000000",
	"411096 66849 411096 521448","-971965 580961 -909294 580961",
	"159038 66849 159038 -1000000","-971965 1000000 725240 1000000",
	"-396104 -420875 -396104 -171934","-909294 521448 628981 521448",
	"-909294 1000000 -909294 -1000000","628981 1000000 -909294 1000000",
	"628981 418077 -396104 418077","-971965 -420875 159038 -420875",
	"1000000 -1000000 -396104 -1000000","-971965 66849 159038 66849",
	"-909294 418077 1000000 418077","-909294 418077 411096 418077",
	"725240 521448 725240 418077","-252927 -1000000 -1000000 -1000000",
	"411096 549481 -1000000 549481","628981 -171934 628981 923907",
	"-1000000 66849 -1000000 521448","-396104 66849 -396104 1000000",
	"628981 -1000000 628981 521448","-971965 521448 -396104 521448",
	"-1000000 418077 1000000 418077","-1000000 521448 -252927 521448",
	"725240 -420875 725240 -1000000","-1000000 549481 -1000000 -420875",
	"159038 521448 -396104 521448","-1000000 521448 -252927 521448",
	"628981 580961 628981 549481","628981 -1000000 628981 521448",
	"1000000 66849 1000000 -171934","-396104 66849 159038 66849",
	"1000000 66849 -396104 66849","628981 1000000 628981 521448",
	"-252927 923907 -252927 580961","1000000 549481 -971965 549481",
	"-909294 66849 628981 66849","-252927 418077 628981 418077",
	"159038 -171934 -909294 -171934","-252927 549481 159038 549481"};
    
    vector<segment> s = parse_string(v);
    merge_segments(s);
    for (int i = 0; i<s.size(); i++)
      {
	cout<<s[i]<<endl;
      }

    cout<<numTimes(v,824759)<<endl;
   }
};


int main() {
  PenLift p;
}
